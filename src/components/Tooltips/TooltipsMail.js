import React, { useRef } from "react";

import { Link } from "react-router-dom";
import Tooltips from "@material-tailwind/react/Tooltips";
import TooltipsContent from "@material-tailwind/react/TooltipsContent";

import Mail from "components/SVG/Mail";

export default function TooltipMail() {
  const buttonRef = useRef();

  return (
    <>
      <Link to="/" className="CustomTooltip" ref={buttonRef}>
        <Mail />
      </Link>
      <Tooltips placement="left" ref={buttonRef}>
        <TooltipsContent>EMail</TooltipsContent>
      </Tooltips>
    </>
  );
}
