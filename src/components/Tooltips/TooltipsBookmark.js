import React, { useRef } from "react";

import { Link } from "react-router-dom";
import Tooltips from "@material-tailwind/react/Tooltips";
import TooltipsContent from "@material-tailwind/react/TooltipsContent";

import Bookmark from "components/SVG/Bookmark";

export default function TooltipBookmark() {
  const buttonRef = useRef();

  return (
    <>
      <Link to="/" className="CustomTooltip" ref={buttonRef}>
        <Bookmark />
      </Link>
      <Tooltips placement="left" ref={buttonRef}>
        <TooltipsContent>Bookmark</TooltipsContent>
      </Tooltips>
    </>
  );
}
