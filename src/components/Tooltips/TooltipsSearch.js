import React, { useRef } from "react";

import { Link } from "react-router-dom";
import Tooltips from "@material-tailwind/react/Tooltips";
import TooltipsContent from "@material-tailwind/react/TooltipsContent";

import Search from "components/SVG/Search";

export default function TooltipSearch() {
  const buttonRef = useRef();

  return (
    <>
      <Link to="/" className="CustomTooltip" ref={buttonRef}>
        <Search />
      </Link>
      <Tooltips placement="left" ref={buttonRef}>
        <TooltipsContent>Search</TooltipsContent>
      </Tooltips>
    </>
  );
}
