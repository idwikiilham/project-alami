import React, { useRef } from "react";

import { Link } from "react-router-dom";
import Tooltips from "@material-tailwind/react/Tooltips";
import TooltipsContent from "@material-tailwind/react/TooltipsContent";

import CartFill from "components/SVG/CartFill";

export default function TooltipCart() {
  const buttonRef = useRef();

  return (
    <>
      <Link to="/" className="CustomTooltip" ref={buttonRef}>
        <CartFill />
      </Link>
      <Tooltips placement="left" ref={buttonRef}>
        <TooltipsContent>Cart</TooltipsContent>
      </Tooltips>
    </>
  );
}
