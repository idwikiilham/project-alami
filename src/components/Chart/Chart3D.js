import React from "react";

export default function Chart3D() {
  return (
    <div className="mx-auto">
      <div className="mx-auto">
        <div className="donut-chart chart1">
          <div className="bg-green-500 quad one"></div>
          <div className="bg-green-500 quad two"></div>
          <div className="bg-green-500 quad three"></div>
          <div className="bg-green-500 quad top"></div>
          <div className="chart-center"></div>
        </div>
      </div>
      <p class="text-xl text-center font-semibold mt-4">3D</p>
    </div>
  );
}
