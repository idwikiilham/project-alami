import React from "react";

export default function ChartPHP() {
  return (
    <div className="mx-auto">
      <div className="mx-auto">
        <div className="donut-chart chart2">
          <div className="bg-green-500 quad one"></div>
          <div className="bg-green-500 quad two"></div>
          <div className="bg-green-500 quad three"></div>
          <div className="bg-green-500 quad four"></div>
          <div className="bg-green-500 quad five"></div>
          <div className="bg-green-500 quad top"></div>
          <div className="chart-center"></div>
        </div>
      </div>
      <p class="text-xl text-center font-semibold mt-4">PHP</p>
    </div>
  );
}
