import React from "react";

const Waves = (props) => {
  return (
    <svg
      className="waves"
      shapeRendering="auto"
      preserveAspectRatio="none"
      viewBox="0 24 150 28"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <defs>
        <path
          id="a"
          d="m-160 44c30 0 58-18 88-18s58 18 88 18 58-18 88-18 58 18 88 18v44h-352z"
        />
      </defs>
      <g className="waves-parallax">
        <use x="48" fill="rgba(255,255,255,0.7" xlinkHref="#a" />
        <use x="48" y="3" fill="rgba(255,255,255,0.5)" xlinkHref="#a" />
        <use x="48" y="5" fill="rgba(255,255,255,0.3)" xlinkHref="#a" />
        <use x="48" y="7" fill="#fff" xlinkHref="#a" />
      </g>
    </svg>
  );
};

export default Waves;
