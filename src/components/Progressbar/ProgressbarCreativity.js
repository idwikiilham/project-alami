import React from "react";
import Progress from "@material-tailwind/react/Progress";

export default function ProgressbarCreativity() {
  return (
    <>
      <div className="">
        <Progress color="green" value="90" percentage={true} />
        <p class="text-xl font-semibold mt-4">Creativity</p>
      </div>
    </>
  );
}
