import React from "react";
import Progress from "@material-tailwind/react/Progress";

export default function ProgressbarCooking() {
  return (
    <>
      <div className="">
        <Progress color="green" value="65" percentage={true} />
        <p class="text-xl font-semibold mt-4">Cooking</p>
      </div>
    </>
  );
}
