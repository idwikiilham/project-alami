import React from "react";
import Progress from "@material-tailwind/react/Progress";

export default function ProgressbarPHP() {
  return (
    <>
      <div className="">
        <Progress color="green" value="85" percentage={true} />
        <p class="text-xl font-semibold mt-4">PHP</p>
      </div>
    </>
  );
}
