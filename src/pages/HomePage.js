import React from "react";

import Header from "parts/Header";
import Hero from "parts/HomePage/Hero";
import CateringCategories from "parts/HomePage/CateringCategories";
import CateringProducts from "parts/HomePage/CateringProducts";
import WidgetSide from "parts/HomePage/WidgetSide";
import Footer from "parts/Footer";

import Document from "parts/Document";

export default function HomePage() {
  return (
    <Document>
      <Header theme="white" position="absolute" />
      <Hero />
      <CateringCategories />
      <CateringProducts />
      <WidgetSide />
      <Footer />
    </Document>
  );
}
