import React from "react";
import data from "assets/data";

import Document from "parts/Document";

import AboutCard from "parts/Portofolio/AboutCard";
import Skills from "parts/Portofolio/Skills";
import DownloadCard from "parts/Portofolio/DownloadCard";

import ProgressbarCooking from "components/Progressbar/ProgressbarCooking";
import ProgressbarPHP from "components/Progressbar/ProgressbarPHP";
import ProgressbarMarketing from "components/Progressbar/ProgressbarMarketing";
import ProgressbarCreativity from "components/Progressbar/ProgressbarCreativity";

import ChartMarketing from "components/Chart/ChartMarketing";
import ChartPhoto from "components/Chart/ChartPhoto";
import ChartPHP from "components/Chart/ChartPHP";
import Chart3D from "components/Chart/Chart3D";

import Header from "parts/Header";
import WidgetSide from "parts/HomePage/WidgetSide";
import Footer from "parts/Footer";

function Portofolio() {
  return (
    <Document>
      <Header theme="black" />
      <div className="min-h-screen py-10 px-3 sm:px-5 bg-gray-100">
        <div className="container mx-auto">
          <div className="grid grid-rows-2 grid-cols-8 gap-4">
            <AboutCard
              name={data.name}
              title={data.title}
              description={data.description}
            />
            <Skills skills={data.skills} />
            <DownloadCard />
          </div>
          <div className="grid grid-rows-1 grid-cols-1 gap-4">
            <div className="text-center py-20">
              <h1 className="text-3xl md:text-6xl font-light font-grey-300 font-Delafield pb-2">
                Skills
              </h1>
              <h1 className="text-3xl md:text-3xl leading-tight font-bold pb-5">
                Lorem ipsum dolor sit amet
              </h1>
              <p className="pb-5">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
              </p>
            </div>
          </div>
          <div className="grid grid-rows-1 grid-cols-1 md:grid-cols-4 gap-4">
            <ChartMarketing />
            <ChartPhoto />
            <ChartPHP />
            <Chart3D />
          </div>
          <div className="grid grid-rows-4 md:grid-rows-1 grid-cols-1 md:grid-cols-2 gap-4 pt-20">
            <ProgressbarCreativity />
            <ProgressbarCooking />
            <ProgressbarPHP />
            <ProgressbarMarketing />
          </div>
        </div>
      </div>
      <WidgetSide />
      <Footer />
    </Document>
  );
}
export default Portofolio;
