import React from "react";

import { Link } from "react-router-dom";

export default function PageErrorMessage({
  title = "404 NOT FOUND",
  body = "The page you are looking for does not exist.",
}) {
  return (
    <section className="mt-28">
      <div className="container mx-auto min-h-screen">
        <div className="flex flex-col items-center justify-center">
          <div className="w-full md:w-4/12 text-center">
            <img
              src="/images/content/404.png"
              className="max-w-xs mx-auto pb-10"
              alt="404 NOT FOUND"
            />
            <h2 className="text-3xl font-semibold mb-6">{title}</h2>
            <p className="text-lg mb-12">{body}</p>
            <Link
              to="/"
              className="text-white bg-green-400 focus:outline-none w-full py-3 rounded-full text-lg focus:text-black transition-all duration-200 px-8 cursor-pointer"
            >
              Back to homepage
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
}
