import React, { useState } from "react";
import { Link } from "react-router-dom";

import CartBorder from "components/SVG/CartBorder";
import Search from "components/SVG/Search";
export default function Header({ theme, position }) {
  const [toggleMainMenu, setToggleMainMenu] = useState(false);

  return (
    <header
      className={[
        position,
        "w-full bg-white text-black shadow-xl sticky top-0 z-50 z-40 px-4",
      ].join(" ")}
    >
      <div className="container mx-auto py-5">
        <div className="flex flex-stretch items-center">
          <div className="w-56 items-center flex">
            <Link to="/">
              <img src="/images/content/logo.svg" alt="Alami" />
            </Link>
          </div>
          <div className="w-full"></div>
          <div className="w-auto">
            <ul
              className={[
                "fixed bg-white inset-0 flex flex-col items-center justify-center md:visible md:flex-row md:bg-transparent md:absolute md:opacity-100 md:flex md:items-center",
                toggleMainMenu
                  ? "opacity-100 z-30 visible"
                  : "invisible opacity-0",
              ].join(" ")}
            >
              <li className="mx-3 py-6 md:py-0">
                <Link
                  to="/"
                  className={[
                    "hover:underline",
                    theme === "white"
                      ? "text-black md:text-black"
                      : "text-black md:text-black",
                  ].join(" ")}
                >
                  Homepage
                </Link>
              </li>
              <li className="mx-3 py-6 md:py-0">
                <Link
                  to="/demos"
                  className={[
                    "hover:underline",
                    theme === "white"
                      ? "text-black md:text-black "
                      : "text-white md:text-black",
                  ].join(" ")}
                >
                  Demos
                </Link>
              </li>
              <li className="mx-3 py-6 md:py-0">
                <Link
                  to="/pages"
                  className={[
                    "hover:underline",
                    theme === "white"
                      ? "text-black md:text-black "
                      : "text-white md:text-black",
                  ].join(" ")}
                >
                  Pages
                </Link>
              </li>
              <li className="mx-3 py-6 md:py-0 hoverable">
                <Link
                  to="/portofolio"
                  className={[
                    "hover:underline",
                    theme === "white"
                      ? "text-black md:text-black "
                      : "text-white md:text-black",
                  ].join(" ")}
                >
                  Portofolio
                </Link>
                <div className="p-6 mega-menu mb-16 sm:mb-0 shadow-xl">
                  <div className="container mx-auto w-full flex flex-wrap justify-between mx-2 pt-10">
                    <ul className="px-4 w-full sm:w-1/2 lg:w-1/3 border-gray-600 pb-6 pt-6 lg:pt-3">
                      <div className="flex items-center">
                        <h3 className="font-bold text-xl text-black text-bold mb-2">
                          Grid minimal
                        </h3>
                      </div>
                      <div className="flex items-center py-3">
                        <ul className={"pl-0"}>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              2 Columns
                            </Link>
                          </li>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              3 Columns
                            </Link>
                          </li>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              4 Columns
                            </Link>
                          </li>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              Full Columns
                            </Link>
                          </li>
                        </ul>
                      </div>
                    </ul>
                    <ul className="px-4 w-full sm:w-1/2 lg:w-1/3 border-gray-600 pb-6 pt-6 lg:pt-3">
                      <div className="flex items-center">
                        <h3 className="font-bold text-xl text-black text-bold mb-2">
                          Grid minimal
                        </h3>
                      </div>
                      <div className="flex items-center py-3">
                        <ul className={"pl-0"}>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              2 Columns
                            </Link>
                          </li>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              3 Columns
                            </Link>
                          </li>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              4 Columns
                            </Link>
                          </li>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              Full Columns
                            </Link>
                          </li>
                        </ul>
                      </div>
                    </ul>
                    <ul className="px-4 w-full sm:w-1/2 lg:w-1/3 border-gray-600 pb-6 pt-6 lg:pt-3">
                      <div className="flex items-center">
                        <h3 className="font-bold text-xl text-black text-bold mb-2">
                          Grid minimal
                        </h3>
                      </div>
                      <div className="flex items-center py-3">
                        <ul className={"pl-0"}>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              2 Columns
                            </Link>
                          </li>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              3 Columns
                            </Link>
                          </li>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              4 Columns
                            </Link>
                          </li>
                          <li className="py-2 md:py-2">
                            <Link
                              to="/portofolio"
                              className="text-black bold hover:text-blue-300"
                            >
                              Full Columns
                            </Link>
                          </li>
                        </ul>
                      </div>
                    </ul>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <div className="w-auto">
            <ul className="items-center flex">
              <li className="ml-6">
                <Link
                  className={[
                    "cart flex items-center justify-center w-8 h-8",
                    theme === "white"
                      ? "text-black md:text-black"
                      : "text-black md:text-black",
                  ].join(" ")}
                  to="#"
                >
                  <CartBorder />
                </Link>
              </li>
              <li className="ml-6 hidden md:block">
                <form action="#" className="w-48">
                  <label className="relative w-full">
                    <input
                      type="text"
                      className="bg-gray-300 rounded-full py-3 px-5 w-full focus:outline-none"
                      placeholder="Search ..."
                    />
                    <button className="bg-green-400 absolute rounded-full right-0 p-3 text-white">
                      <Search />
                    </button>
                  </label>
                </form>
              </li>
              <li className="ml-6 block md:hidden text-black">
                <Search />
              </li>
              <li className="ml-6 block md:hidden">
                <button
                  className={[
                    "flex z-50 items-center justify-center w-8 h-8 text-black md:text-black focus:outline-none",
                    toggleMainMenu ? "fixed top-0 right-0" : "relative",
                    theme === "white"
                      ? "text-black md:text-black"
                      : "text-black md:text-black",
                  ].join(" ")}
                  onClick={() => setToggleMainMenu((prev) => !prev)}
                >
                  <svg
                    className="fill-current"
                    width="18"
                    height="17"
                    viewBox="0 0 18 17"
                  >
                    <path d="M15.9773 0.461304H1.04219C0.466585 0.461304 0 0.790267 0 1.19609C0 1.60192 0.466668 1.93088 1.04219 1.93088H15.9773C16.5529 1.93088 17.0195 1.60192 17.0195 1.19609C17.0195 0.790208 16.5529 0.461304 15.9773 0.461304Z" />
                    <path d="M15.9773 7.68802H1.04219C0.466585 7.68802 0 8.01698 0 8.42281C0 8.82864 0.466668 9.1576 1.04219 9.1576H15.9773C16.5529 9.1576 17.0195 8.82864 17.0195 8.42281C17.0195 8.01692 16.5529 7.68802 15.9773 7.68802Z" />
                    <path d="M15.9773 14.9147H1.04219C0.466585 14.9147 0 15.2437 0 15.6495C0 16.0553 0.466668 16.3843 1.04219 16.3843H15.9773C16.5529 16.3843 17.0195 16.0553 17.0195 15.6495C17.0195 15.2436 16.5529 14.9147 15.9773 14.9147Z" />
                  </svg>
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
}
