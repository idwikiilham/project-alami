import useScrollAnchor from "helpers/hooks/useScrollAnchor";
import useScrollToTop from "helpers/hooks/useScrollToTop";

export default function Documents({ children }) {
  useScrollAnchor();
  useScrollToTop();
  return children;
}
