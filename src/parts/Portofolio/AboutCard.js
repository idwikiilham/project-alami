import React from "react";
import profile from "../../assets/images/profile.png";

function AboutCard({ name, title, description }) {
  return (
    <div className="relative card col-span-9 row-span-2 md:col-span-2 md:row-span-2 justify-center max-w-xs mx-auto bg-white shadow-xl rounded-xl p-5">
      <div className="">
        <img
          className="w-32 mx-auto shadow-xl rounded-full"
          src={profile}
          alt="Profile face"
        />
      </div>
      <div className="text-center mt-5">
        <p className="text-xl sm:text-2xl font-semibold text-gray-900">
          {name}
        </p>
        <p className="text-xs sm:text-base text-gray-600 pt-2 pb-4 px-5 w-auto inline-block border-b-2">
          {title}
        </p>
        <p className="text-xs pt-4">{description}</p>
      </div>
    </div>
  );
}

export default AboutCard;
