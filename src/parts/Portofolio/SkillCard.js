import React from "react";

const SkillCard = ({ skillName, skillDesc, skillIcon }) => {
  return (
    <div
      className="relative card col-span-9 row-span-1 md:col-span-4 md:row-span-1 mx-auto text-center p-5 rounded-xl border-2 border-gray-300"
      style={{ height: 180 }}
    >
      {skillIcon}
      <p className="text-xl font-semibold mt-4">{skillName}</p>
      <p className="">{skillDesc}</p>
    </div>
  );
};

export default SkillCard;
