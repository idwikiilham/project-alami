import React from "react";
import SkillCard from "./SkillCard";

function Skills({ skills }) {
  return (
    <div className="relative card col-span-9 row-span-1 md:col-span-4 md:row-span-2 grid grid-rows-2 grid-cols-8 gap-4">
      {skills.map((skill) => {
        return (
          <SkillCard
            skillName={skill.skillName}
            skillIcon={skill.skillIcon}
            skillDesc={skill.skillDesc}
          />
        );
      })}
    </div>
  );
}

export default Skills;
