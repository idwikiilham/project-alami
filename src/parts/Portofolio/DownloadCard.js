import React from "react";
import RightArrow from "components/SVG/RightArrow";

function DownloadCard({ name, title, description }) {
  return (
    <div className="relative card col-span-9 row-span-1 md:col-span-2 md:row-span-2 justify-center mx-auto p-5">
      <div className="text-left mt-5">
        <p className="text-xl font-semibold mt-4">Develop</p>
        <p className="text-xs pt-4">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
          <br />
          <br />
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
          dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est
          laborum.
        </p>
        <a
          href="#CateringCategories"
          className="bg-green-400 text-white hover:bg-black hover:text-white rounded-full px-8 py-3 mt-4 inline-flex items-center flex-none transition duration-200"
        >
          Download CV <RightArrow />
        </a>
      </div>
    </div>
  );
}

export default DownloadCard;
