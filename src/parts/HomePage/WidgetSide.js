import React from "react";

import TooltipCart from "components/Tooltips/TooltipsCart";
import TooltipSearch from "components/Tooltips/TooltipsSearch";
import TooltipMail from "components/Tooltips/TooltipsMail";
import TooltipBookmark from "components/Tooltips/TooltipsBookmark";
import TooltipPhone from "components/Tooltips/TooltipsPhone";

export default function WidgetSide() {
  return (
    <>
      <div className="WidgetSideWrapper hidden md:fixed md:block top-0 bottom-0 rounded-br-sm rounded-bl-sm">
        <div className="WidgetSideInside bg-white">
          <TooltipCart />
          <TooltipSearch />
          <TooltipMail />
          <TooltipBookmark />
          <TooltipPhone />
        </div>
      </div>
    </>
  );
}
