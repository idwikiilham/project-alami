import React from "react";

import RightArrow from "components/SVG/RightArrow";
import Waves from "components/SVG/Waves";

export default function Hero() {
  return (
    <section className="flex items-center hero">
      <div className="w-full inset-0 md:relative md:w-1/1">
        <div className="relative hero-image">
          <div className="overlay inset-0 bg-black opacity-35 z-10"></div>
          <div className="overlay right-0 bottom-0 md:inset-0">
            <div className="h-full z-20 inset-0 relative w-1/1 text-center text-white flex flex-col justify-center hero-caption p-5">
              <h1 className="text-3xl md:text-6xl font-light font-Delafield pb-2">
                Catering should be an experience
              </h1>
              <h1 className="text-3xl md:text-6xl leading-tight font-bold pb-5">
                We use only the finest and
                <br className="hidden md:block" /> freshest ingredients
              </h1>
              <p className="pb-5">
                At Sway catering we know that food is an important part of life.
                <br className="hidden md:block" />
                If the meal is not perfect, your event cannot be perfect.
              </p>
              <div>
                <a
                  href="#CateringCategories"
                  className="bg-green-400 text-white hover:bg-black hover:text-white rounded-full px-8 py-3 mt-4 inline-flex items-center flex-none transition duration-200"
                >
                  Request a Quote <RightArrow />
                </a>
              </div>
            </div>
          </div>
          <img
            src="images/content/hero_image.jpg"
            alt="hero 1"
            className="absolute inset-0 md:relative w-full h-full object-cover object-center"
          />
          <div className="kd-row-separator kd-row-separator-bottom separator-height-large">
            <Waves />
          </div>
        </div>
      </div>
    </section>
  );
}
