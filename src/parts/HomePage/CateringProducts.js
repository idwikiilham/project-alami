import React, { useEffect, useRef } from "react";

import { Link } from "react-router-dom";

import Carousel from "components/Carousel";

import useAsync from "helpers/hooks/useAsync";
import fetch from "helpers/fetch";
import "helpers/format/currency";

import CartFill from "components/SVG/CartFill";

function Loading() {
  return Array(6)
    .fill()
    .map((_, index) => {
      return (
        <div className="px-4 relative card group" key={index}>
          <div
            className="rounded-xl bg-gray-300 overflow-hidden card-shadow relative"
            style={{ width: 287, height: 386 }}
          ></div>
          <div className="w-24 h-3 bg-gray-300 mt-3 rounded-full"></div>
          <div className="w-36 h-3 bg-gray-300 mt-2 rounded-full"></div>
        </div>
      );
    });
}

export default function CateringProducts() {
  const { data, error, run, isLoading } = useAsync();

  const refContainer = useRef(null);

  useEffect(() => {
    run(fetch({ url: "/api/products/?page=1&limit=10" }));
  }, [run]);

  return (
    <section className="bg-gray-100 flex flex-col py-16">
      <div className="container mx-auto mb-4">
        <div className="text-center py-10">
          <h1 className="text-3xl md:text-6xl font-light font-grey-300 font-Delafield pb-2">
            Our Porducts
          </h1>
          <h1 className="text-3xl md:text-3xl leading-tight font-bold pb-5">
            Visit our online store right now
          </h1>
        </div>
      </div>
      <div className="overflow-x-hidden px-4" id="carousel">
        <div className="container mx-auto" ref={refContainer}></div>
        {/* <!-- <div className="overflow-hidden z-10"> --> */}

        {isLoading ? (
          <div
            className="flex -mx-4 flex-row relative"
            style={{
              paddingLeft:
                refContainer.current?.getBoundingClientRect?.()?.left - 16 || 0,
            }}
          >
            <Loading />
          </div>
        ) : error ? (
          JSON.stringify(error)
        ) : data.data.length === 0 ? (
          "No Product Found"
        ) : (
          <Carousel refContainer={refContainer}>
            {data.data.map((item) => {
              return (
                <div className="px-4 relative card group" key={item.id}>
                  <div
                    className="rounded-xl overflow-hidden card-shadow relative"
                    style={{ width: 287, height: 386 }}
                  >
                    <div className="absolute opacity-0 group-hover:opacity-100 transition duration-200 flex items-center justify-center w-full h-full bg-black bg-opacity-35">
                      <div className="bg-white text-black rounded-full w-16 h-16 flex items-center justify-center">
                        <CartFill />
                      </div>
                    </div>
                    <img
                      src={`/images/content/products/${item.imageUrl}`}
                      alt=""
                      className="w-full h-full object-cover object-center"
                    />
                  </div>
                  <h5 className="text-lg font-semibold mt-4">{item.title}</h5>
                  <span className="">{item.price.currency()}</span>
                  <Link to={`#`} className="stretched-link">
                    {/* <!-- fake children --> */}
                  </Link>
                </div>
              );
            })}
          </Carousel>
        )}
      </div>
    </section>
  );
}
