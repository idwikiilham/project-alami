import React, { useEffect } from "react";

import useAsync from "helpers/hooks/useAsync";
import fetch from "helpers/fetch";

function Loading({ ratio = {} }) {
  const dummy = [
    {
      id: 1,
      ratio: {
        default: "1/9",
        md: "1/4",
      },
    },
    {
      id: 2,
      ratio: {
        default: "1/9",
        md: "2/2",
      },
    },
    {
      id: 3,
      ratio: {
        default: "1/9",
        md: "2/3",
      },
    },
    {
      id: 4,
      ratio: {
        default: "1/9",
        md: "1/4",
      },
    },
  ];

  return dummy.map((item, index) => {
    return (
      <div
        key={item.id}
        className={`relative card ${
          ratio?.wrapper.default?.[item.ratio.default]
        } ${ratio?.wrapper.md?.[item.ratio.md]}`}
        style={{ height: index === 0 ? 180 : "auto" }}
      >
        <div className="bg-gray-300 rounded-lg w-full h-full">
          <div className={`overlay ${ratio?.meta?.[item.ratio.md]}`}>
            <div className="w-24 h-3 bg-gray-400 mt-3 rounded-full"></div>
            <div className="w-36 h-3 bg-gray-400 mt-2 rounded-full"></div>
          </div>
        </div>
      </div>
    );
  });
}

export default function CateringCategories() {
  const { data, run, isLoading } = useAsync();

  useEffect(() => {
    run(fetch({ url: "/api/categories/?page=1&limit=4" }));
  }, [run]);

  const ratioClassNames = {
    wrapper: {
      default: {
        "1/9": "col-span-9 row-span-1",
      },
      md: {
        "1/4": "md:col-span-4 md:row-span-1",
        "2/2": "md:col-span-2 md:row-span-2",
        "2/3": "md:col-span-3 md:row-span-2",
      },
    },
    meta: {
      "1/9":
        "left-0 top-0 bottom-0 flex justify-center flex-col pl-48 md:pl-72",
      "1/4":
        "left-0 top-0 bottom-0 flex justify-center flex-col pl-48 md:pl-72",
      "2/2":
        "inset-0 md:bottom-auto flex justify-center md:items-center flex-col pl-48 md:pl-0 pt-0 md:pt-12",
      "2/3":
        "inset-0 md:bottom-auto flex justify-center md:items-center flex-col pl-48 md:pl-0 pt-0 md:pt-12",
    },
  };

  return (
    <section className="flex py-16 px-4" id="CateringCategories">
      <div className="container mx-auto">
        <div className="text-center py-20">
          <h1 className="text-3xl md:text-6xl font-light font-grey-300 font-Delafield pb-2">
            Catering services in New York
          </h1>
          <h1 className="text-3xl md:text-3xl leading-tight font-bold pb-5">
            We specialize in corporate and private events
          </h1>
          <p className="pb-5">
            With 20 years of experience, we promise you the freshest, local
            ingredients, hand-crafted
            <br className="" />
            cooking sprinkled with our unique whimsical elegance and exceptional
            service.
          </p>
        </div>

        <div className="grid grid-rows-2 grid-cols-9 gap-4">
          {isLoading ? (
            <Loading ratio={ratioClassNames} />
          ) : (
            data.data.map((item, index) => {
              return (
                <div
                  key={item.id}
                  className={`relative card ${
                    ratioClassNames?.wrapper.default?.[item.ratio.default]
                  } ${ratioClassNames?.wrapper.md?.[item.ratio.md]}`}
                  style={{ height: index === 0 ? 180 : "auto" }}
                >
                  <div className="card-shadow rounded-xl">
                    <div
                      className={`z-50 h-full overlay ${
                        ratioClassNames?.meta?.[item.ratio.md]
                      }`}
                    >
                      <h1 className="text-lg font-semibold text-white">
                        {item.title}
                      </h1>
                    </div>
                    <div className="overlay inset-0 bg-black opacity-35 z-10 rounded-xl"></div>
                    <img
                      src={`/images/content/categories/${item.imageUrl}`}
                      alt={item.title}
                      className="w-full h-full object-cover object-center overlay overflow-hidden rounded-xl"
                    />
                  </div>
                </div>
              );
            })
          )}
        </div>
      </div>
    </section>
  );
}
