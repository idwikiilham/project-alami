import "assets/css/app.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import HomePage from "pages/HomePage";
import Portofolio from "pages/Portofolio";
import NotFound from "pages/NotFound";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/portofolio" component={Portofolio} />
        <Route path="*" component={NotFound} />
      </Switch>
    </Router>
  );
}

export default App;
