# Alami Sharia Front-End Test

Project Front-End Test with ReactJS and TailwindCSS

## Getting Started

First, run the development server:

```bash
npm install
# and
npm start
```
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
## JS Stack

- ReactJS
- TailwindCSS

## Feature

- Homepage
- WidgetSideBar
- MegaMenu
- Portofolio Page
